# Configure the Azure Provider
terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "~> 2.65"
    }
  }
  required_version = ">= 0.14.9"
}

provider "azurerm" {
  features {}
}

# Toutes les ressources de "haut niveau" ont été créées à la main
variable "loc" {
  type = string
  default = "westeurope"
}
data "azurerm_resource_group" "resourcegroup" {
    name =  "Groupe4_Tomcat"
}

data "azurerm_virtual_network" "vnet" {
  name = "VN_Tomcat"
  resource_group_name = data.azurerm_resource_group.resourcegroup.name
}
data "azurerm_subnet" "clustersubnet" {
  name = "cluster"
  virtual_network_name = data.azurerm_virtual_network.vnet.name
  resource_group_name = data.azurerm_resource_group.resourcegroup.name
}
# Localisation clef ssh pour parler au cluster
variable "sshfilepub" {
  type = string
  default = "/home/pa/.ssh/id_rsa.pub"
}
variable "sshfilepriv" {
  type = string
  default = "/home/pa/.ssh/id_rsa"
}
# Nombre de master nodes
variable "masternumber" {
  default = 1
}
# Nombre de slave nodes
variable "slavenumber" {
  default = 2
}

# Setup clef ssh pour que le contrôle puisse parler au vms
resource "azurerm_ssh_public_key" "ssh_tomcat" {
  name = "sshtonodes"
  resource_group_name = data.azurerm_resource_group.resourcegroup.name
  location = var.loc
  public_key = file(var.sshfilepub)
}

# Création des slave nodes
resource "azurerm_network_interface" "nislaves" {
  count = var.slavenumber
  name = "nislavenodes${count.index}"
  resource_group_name = data.azurerm_resource_group.resourcegroup.name
  location = var.loc

  ip_configuration {
    name = "internal"
    subnet_id = data.azurerm_subnet.clustersubnet.id
    private_ip_address_allocation = "Dynamic"
  }
}
resource "azurerm_linux_virtual_machine" "slave_node" {
  count = var.slavenumber
  name = "slavenode${count.index}"
  resource_group_name = data.azurerm_resource_group.resourcegroup.name
  location = var.loc
  size = "Standard_B1s"

  admin_username="clustercontroller"
  admin_ssh_key {
    username = "clustercontroller"
    public_key = azurerm_ssh_public_key.ssh_tomcat.public_key
  }

  os_disk {
    caching = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  network_interface_ids = [
    azurerm_network_interface.nislaves.*.id[count.index],
  ]

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-focal"
    sku       = "20_04-lts-gen2"
    version   = "20.04.202203020"
  }
  tags = {
    ansible-group = "slaves"
  }
}

# Création du master node
resource "azurerm_network_interface" "nimasters" {
  count = var.masternumber
  name = "nimasternodes${count.index}"
  resource_group_name = data.azurerm_resource_group.resourcegroup.name
  location = var.loc

  ip_configuration {
    name = "internal"
    subnet_id = data.azurerm_subnet.clustersubnet.id
    private_ip_address_allocation = "Dynamic"
  }
}
resource "azurerm_linux_virtual_machine" "master_node" {
  count = var.masternumber
  name = "masternode${count.index}"
  resource_group_name = data.azurerm_resource_group.resourcegroup.name
  location = var.loc
  size = "Standard_B2s"

  admin_username="clustercontroller"
  admin_ssh_key {
    username = "clustercontroller"
    public_key = azurerm_ssh_public_key.ssh_tomcat.public_key
  }

  os_disk {
    caching = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  network_interface_ids = [
    azurerm_network_interface.nimasters.*.id[count.index],
  ]

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-focal"
    sku       = "20_04-lts-gen2"
    version   = "20.04.202205050"
  }
  tags = {
    ansible-group = "masters"
  }
}