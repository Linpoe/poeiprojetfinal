# Configure the Azure Provider
terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "~> 2.65"
    }
  }

  required_version = ">= 0.14.9"
}

provider "azurerm" {
  features {}
}

#variable "resourcegroup" {
#  type = string
#  default = "Groupe4_Tomcat"
#}

variable "grouplocation" {
  type = string
  default = "westeurope"
}

variable "ssh_key" {
  type = string
  default = "/home/pierreantoine/.ssh/terraform_key.pub"
}

resource "azurerm_resource_group" "rg_tomcat" {
  name = "Groupe4_Tomcat"
  location = var.grouplocation
}

resource "azurerm_public_ip" "ip_tomcat" {
  name = "ipmachinetomcat"
  resource_group_name = azurerm_resource_group.rg_tomcat.name
  location = var.grouplocation
  allocation_method = "Static"
}

resource "azurerm_network_security_group" "nsg_tomcat" {
  name = "nsgtomcat"
  resource_group_name = azurerm_resource_group.rg_tomcat.name
  location = var.grouplocation
}

resource "azurerm_virtual_network" "vn_tomcat" {
  name = "vntomcat"
  resource_group_name = azurerm_resource_group.rg_tomcat.name
  location = var.grouplocation
  address_space = ["10.0.0.0/16"]
}

resource "azurerm_subnet" "subnet_tomcat" {
  name = "subnettomcat"
  resource_group_name = azurerm_resource_group.rg_tomcat.name
  virtual_network_name = azurerm_virtual_network.vn_tomcat.name
  address_prefixes = ["10.0.1.0/24"]
}

resource "azurerm_ssh_public_key" "ssh_tomcat" {
  name = "sshtomcat"
  resource_group_name = azurerm_resource_group.rg_tomcat.name
  location = var.grouplocation
  public_key = file(var.ssh_key)
}

resource "azurerm_network_interface" "ni_tomcat" {
  name = "nitomcat"
  resource_group_name = azurerm_resource_group.rg_tomcat.name
  location = var.grouplocation

  ip_configuration {
    name = "internal"
    subnet_id = azurerm_subnet.subnet_tomcat.id
    private_ip_address_allocation = "Dynamic"

    public_ip_address_id = azurerm_public_ip.ip_tomcat.id
  }
}

resource "azurerm_linux_virtual_machine" "vm_tomcat" {
  name = "vmtomcat"
  resource_group_name = azurerm_resource_group.rg_tomcat.name
  location = var.grouplocation
  size = "Standard_F2"
  admin_username = "pa"
  network_interface_ids = [
    azurerm_network_interface.ni_tomcat.id,
  ]

 admin_ssh_key {
    username = "pa"
    public_key = azurerm_ssh_public_key.ssh_tomcat.public_key
  }

  os_disk {
    caching = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer = "UbuntuServer"
    sku = "18.04-LTS"
    version = "latest"
  }
}


resource "local_file" "host_file" {
  content = azurerm_public_ip.ip_tomcat.ip_address
  filename = "tomcat_host"
}




