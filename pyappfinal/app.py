from flask import Flask,render_template,request,Response

app = Flask(__name__)


@app.route('/')
def index():
    val = 100
    return render_template("index.html" ,valeur=val)

@app.route('/test')
def test():
    return render_template("test.html")


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=80, debug=False)