Contents

introduction

L’entreprise Nano Corporation a besoin de trois applications Web :
•	Un blog statique , solution retenue Jekyll
•	Application JAVA Tomcat
•	Web serveur en python.
La solution Cloud retenue : Azure. Le cloud azure ne servira que de ressources VM.
Nano Corporation souhaiterait un déploiement de l’infrastructure le plus dynamique possible, souple , automatisé.
La présence de chaîne CI/CD est fortement recommandé pour les trois applications web.
les outils :
•	Gitlab - CI/CD
•	Serveurs LAMP
•	K8s
•	Jenkins
•	Containerisation
•	Ansible

1.  Premier sites Blog : type statique avec le framework Jekyll
Le déploiement prévoit ;
	la mise en place d’outils : zsh, sudo, git, htop
	la création d’un utilisateur (dev) qui aura acces a DocumentRoot du site via ssh+clefs , sudo sera requis.
Les fichiers composants le site seront sur le serveur Gitlab, un commit du développeur déclenche la mise à jour du site web. (Utilisation de CI/CD)

2.  Un deuxième site lui sera composé d’une application Tomcat : (application WAR fournit avec un dump SQL , phpmyadmin + serveur Tomcat + mysql) . Une haute disponibilité est requise.

3.  Un troisième site, une application web python ”containerisée cette application sera sur le dépôt Gitlab. Un commit des développeurs déclenchera via Jenkins un build de l’image Docker. Il faudra ensuite déployer l’application. (test , livraison).
L’application python servira de serveur Web et affichera une page web par défaut.(python simple ou Django)

NB1:
Un accés Vpn sera nécessaire pour l’administration des serveurs applicatifs. La base de données du serveur Tomcat devra être sauvegardé régulierement. Les sites web devront utiliser le protocole https.
 
NB2:
une documentation sera demandée , pourquoi ne pas la produire en markdown et la convertir en LaTeX de façon automatique (Gitlab-CI).
